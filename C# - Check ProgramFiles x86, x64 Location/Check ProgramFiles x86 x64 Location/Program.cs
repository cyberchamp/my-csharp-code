﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Check_ProgramFiles_x86_x64_Location
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(Checkx86x64());
            Console.ReadLine();
        }

        static string Checkx86x64()
        {
            if (8 == IntPtr.Size || (!String.IsNullOrEmpty(Environment.GetEnvironmentVariable("PROCESSOR_ARCHITEW6432"))))
            {
                return Environment.GetEnvironmentVariable("ProgramFiles(x86)");
            }

            return Environment.GetEnvironmentVariable("ProgramFiles");
        }
    }

}

