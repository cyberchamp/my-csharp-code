﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CheckExistsFolder
{
    class Program
    {
        static void Main(string[] args)
        {
            string path = System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "/CheckExistsFolder";
            try
            {
                bool folderExists = Directory.Exists(path);
                if (!folderExists)
                {
                    Directory.CreateDirectory(path);

                }
            }
            catch (IOException ioex)
            {
            }
        }
    }
}
